# Nasz projekt testowy

## Procedura tworzenie repo:

1. tworzymy katalog projektu

   `mkdir nazwa`

2. w katalogu inicjujemy gita
3. tworzymy pliki projektu
4. dodajemy pliki do śledzenia
5. commitujemy pliki do lokalnego repozytorium
6. tworzymy repozytorium zdalne
7. podłączamy repo zdalne do lokalnego (synchronizacja)
8. pushujemy lokalny branch master do zdalnego repo
